﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Syncfusion.Licensing;

using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Colosseum
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //uses free lifetime license for movies by Registering a free account
            SyncfusionLicenseProvider.RegisterLicense("OTUzMzVAMzEzNzJlMzEyZTMwaEtpUDIzdXFhOE1Xdjk4U2NpQnRBYmJjMEs4c1duK2NpeTFSNkx6TGREQT0=");

            //we want to display the HomePage as the load page of our application
            MainPage = new NavigationPage(new HomePage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            //app tracking
            AppCenter.Start("ios=1f58d3b1-8d38-401f-86cf-b5acd808e7ee;android=7ccf01f0-848f-475f-8918-70ae35d1b63d;uwp=129952cc-fe15-47f3-b6ba-6e20583986fd", typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
