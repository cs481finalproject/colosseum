﻿using System;

using Xamarin.Forms;

namespace Colosseum.Model
{
    public class MyView : ContentView
    {
        public MyView()
        {
            Content = new Label { Text = "Hello ContentView" };
        }
    }
}

