﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Colosseum.Model;
using Colosseum.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XFGloss;
using ItemTappedEventArgs = Syncfusion.ListView.XForms.ItemTappedEventArgs;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace Colosseum
{
    public partial class LatestMoviesPage : ContentPage
    {
        public ObservableCollection<LatestMovie> LatestMovies;
        private static bool First = true;
        public LatestMoviesPage()
        {
            InitializeComponent();
            var bkgrndGradient = new Gradient()
            {
                Rotation = 150,
                Steps = new GradientStepCollection()
                {
                    new GradientStep(Color.Orange, 0),
                    new GradientStep(Color.Yellow, .25),
                    new GradientStep(Color.LightGoldenrodYellow, .5),
                    new GradientStep(Color.LightYellow, 1)
                }
            };

            ContentPageGloss.SetBackgroundGradient(this, bkgrndGradient);
            LatestMovies = new ObservableCollection<LatestMovie>();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (First)
            {
                ApiServices apiServices = new ApiServices();
                var latestMovies = await apiServices.GetLatestMovies();
                foreach (var latestMovie in latestMovies)
                {
                    LatestMovies.Add(latestMovie);
                }

                LvLatest.ItemsSource = LatestMovies;
            }

            First = false;
        }

        private void LvLatest_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            //Tracking Event E (passed in by parameter)
            Analytics.TrackEvent("e");
            var items = e.ItemData as LatestMovie;
            if (items != null)
            {
                DisplayAlert("Alert", "Please Be Patient: The Video May Take Some Time To Load", "OK");
                Navigation.PushAsync(new VideoPage(items.MovieTrailor));
            }
        }
    }
}
