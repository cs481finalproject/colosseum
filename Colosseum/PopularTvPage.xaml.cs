﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Colosseum.Model;
using Colosseum.Services;
using XFGloss;

namespace Colosseum
{
    public partial class PopularTvPage : ContentPage
    {
        public ObservableCollection<PopularTv> Tv;
        private static bool first = true;
        public PopularTvPage()
        {
            InitializeComponent();
            var bkgrndGradient = new Gradient()
            {
                Rotation = 150,
                Steps = new GradientStepCollection()
                {
                   new GradientStep(Color.Orange, 0),
                   new GradientStep(Color.Yellow, .25),
                   new GradientStep(Color.LightGoldenrodYellow, .5),
                   new GradientStep(Color.Goldenrod, 0.75),
                   new GradientStep(Color.LightYellow, 1)
                }
            };

            ContentPageGloss.SetBackgroundGradient(this, bkgrndGradient);
            Tv = new ObservableCollection<PopularTv>();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            //first is used to make sure that the contents won't be displayed multiple times on page navigation
            if (first)
            {
                ApiServices apiServices = new ApiServices();
                var tvShows = await apiServices.GetPopularTv();
                Array result = tvShows.Results;
                TvPlaying.ItemsSource = result;
            }
            first = false;

        }

    }
}
