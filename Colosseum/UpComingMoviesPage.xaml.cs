﻿using System.Collections.ObjectModel;
using Colosseum.Model;
using Xamarin.Forms;
using Colosseum.Services;
using XFGloss;
using ItemTappedEventArgs = Syncfusion.ListView.XForms.ItemTappedEventArgs;

namespace Colosseum
{

    public partial class UpComingMoviesPage : ContentPage
    {

        public ObservableCollection<UpcomingMovie> UpcomingMovies;
        private static bool First = true;

        public UpComingMoviesPage()
        {
            InitializeComponent();
            var bkgrndGradient = new Gradient()
            {
                Rotation = 150,
                Steps = new GradientStepCollection()
                {
                   new GradientStep(Color.Orange, 0),
                    new GradientStep(Color.Yellow, .25),
                    new GradientStep(Color.LightGoldenrodYellow, .5),
                   // new GradientStep(Color.Goldenrod, 0.75),
                    new GradientStep(Color.LightYellow, 1)
                }
            };

            ContentPageGloss.SetBackgroundGradient(this, bkgrndGradient);
            UpcomingMovies = new ObservableCollection<UpcomingMovie>();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (First)
            {
                ApiServices apiServices = new ApiServices();
                var upComingMovies = await apiServices.GetUpComingMovies();

                foreach (var upComingMovie in upComingMovies)
                {
                    UpcomingMovies.Add(upComingMovie);
                }

                LvUpComing.ItemsSource = UpcomingMovies;
            }

            First = false;

        }

        //upcoming movies page, gets the youtube link to watch the trailor
        private void LvUpComing_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var items = e.ItemData as UpcomingMovie;
            if (items != null)
            {
                DisplayAlert("Alert", "Please Be Patient: The Video May Take Some Time To Load", "OK");
                Navigation.PushAsync(new VideoPage(items.MovieTrailor));
            }
        }
    }
}