﻿ using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Colosseum.Model;
using Colosseum.Services;
using XFGloss;
using Xamarin.Forms;

namespace Colosseum
{
    public partial class NowPlayingMoviesPage : ContentPage
    {
        public ObservableCollection<NowPlayingMovie> NowPlayingMovies;
        private static bool first = true;
        public NowPlayingMoviesPage()
        {
            InitializeComponent();
            var bkgrndGradient = new Gradient()
            {
                Rotation = 150,
                Steps = new GradientStepCollection()
                {
                   new GradientStep(Color.Orange, 0),
                    new GradientStep(Color.Yellow, .25),
                    new GradientStep(Color.LightGoldenrodYellow, .5),
                    //new GradientStep(Color.Goldenrod, 0.75),
                    new GradientStep(Color.LightYellow, 1)
                }
            };

            ContentPageGloss.SetBackgroundGradient(this, bkgrndGradient);
            NowPlayingMovies = new ObservableCollection<NowPlayingMovie>();

        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            //first is used to make sure that the contents won't be displayed multiple times on page navigation
            if(first)
            {
                ApiServices apiServices = new ApiServices();
                var nowPlayingMovies = await apiServices.GetNowPlayingMovies();
                foreach (var nowPlayingMovie in nowPlayingMovies)
                {
                    NowPlayingMovies.Add(nowPlayingMovie);
                }
                LvNowPlaying.ItemsSource = NowPlayingMovies;

                //indicator that tells the user that the page is loading, will stop once movies have been loaded
                BusyIndicator.IsRunning = false;


            }
            first = false;

        }

        private void LvNowplaying_OnItemSelected(Object sender, SelectedItemChangedEventArgs e)
        {
            var selectedMovie = e.SelectedItem as NowPlayingMovie;
            if(selectedMovie != null)
            {
                //DisplayAlert("Alert", "Please Be Patient: The Movie May Take Some Time To Load", "OK");
                Navigation.PushAsync(new NowPlayingDetailPage(selectedMovie));

            }
            ((ListView)sender).SelectedItem = null;
        }
    }
}
