﻿using System;
using System.Collections.Generic;
using Colosseum.Model;

using Xamarin.Forms;

namespace Colosseum
{
    public partial class NowPlayingDetailPage : ContentPage
    {
        private string _trailorLink;
        private string _time1;
        private string _time2;
        private string _time3;
        private string _ticketPrice;

        public NowPlayingDetailPage(NowPlayingMovie nowPlayingMovie)
        {
            InitializeComponent();
            LblMovieName.Text = nowPlayingMovie.MovieName;
            ImgMovieCover.Source = nowPlayingMovie.CoverImage;
            LblDuration.Text = nowPlayingMovie.Duration;
            LblLanguage.Text = nowPlayingMovie.Language;
            LblMovieType.Text = nowPlayingMovie.Genre;
            LblPlayingDate.Text = nowPlayingMovie.PlayingDate.ToShortDateString();
            LblCast.Text = nowPlayingMovie.Cast;
            LblDescription.Text = nowPlayingMovie.Description;
            _trailorLink = nowPlayingMovie.MovieTrailor;
            _time1 = nowPlayingMovie.ShowTime1.ToShortTimeString();//showtime 1
            _time2 = nowPlayingMovie.ShowTime2.ToShortTimeString();//showtime 2
            _time3 = nowPlayingMovie.ShowTime3.ToShortTimeString();//showtime 3
            _ticketPrice = nowPlayingMovie.TicketPrice; //ticket price and qauntity
        }

        //youtube video trailer on tap
        private void PlayVideo_OnTapped(object sender, EventArgs e)
        {
            DisplayAlert("Alert", "Please Be Patient: The Video May Take Some Time To Load", "OK");
            Navigation.PushAsync(new VideoPage(_trailorLink));
        }

        //book ticket button
        private void BtnBookTicket_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new BookTicketPage(ImgMovieCover.Source, LblMovieName.Text, LblPlayingDate.Text, _time1, _time2, _time3, _ticketPrice));
        }

    }
}

