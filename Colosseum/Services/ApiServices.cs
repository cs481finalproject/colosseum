﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Colosseum.Model;
using Newtonsoft.Json;

namespace Colosseum.Services
{
    public class ApiServices
    {
        private string nowPlayingMoviesUrl = "http://colosseum.somee.com/api/NowPlayingMovies";
        private string upComingMoviesUrl = "http://colosseum.somee.com/api/UpComingMovies";
        private string orderApiUrl = "http://colosseum.somee.com/api/Orders";
        private string latestMoviesUrl = "http://colosseum.somee.com/api/LatestMovies";
        private string popularTvUrl = "https://api.themoviedb.org/3/tv/popular?api_key=bf493b164d003dde8cabde88c6558ab0&language=en-US&page=1";

        public async Task<PopularTv> GetPopularTv()
        {
            var client = new HttpClient();
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, popularTvUrl);
            var responseMessage = await client.SendAsync(requestMessage);
            var movieResponse = await responseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<PopularTv>(movieResponse);
        }

        public async Task<List<NowPlayingMovie>> GetNowPlayingMovies()
        {
            var client = new HttpClient();
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get,nowPlayingMoviesUrl);
            requestMessage.Headers.Add("ApiKey", "0c97bf09-4fbc-4395-ad41-d181191b1a52");
            // get the http content in the variable responseMessage from the server i.e requestMessage
            var responseMessage= await client.SendAsync(requestMessage);
            var movieResponse = await responseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<NowPlayingMovie>>(movieResponse);
        }

        public async Task<List<UpcomingMovie>> GetUpComingMovies()
        {
            var client = new HttpClient();
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, upComingMoviesUrl);
            requestMessage.Headers.Add("ApiKey", "0c97bf09-4fbc-4395-ad41-d181191b1a52");
            var responseMessage = await client.SendAsync(requestMessage);
            var movieResponse = await responseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<UpcomingMovie>>(movieResponse);
        }
       
         public async Task<bool> Order(BookTicket bookTicket)
        {
            var client = new HttpClient();
            var json = JsonConvert.SerializeObject(bookTicket);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            content.Headers.Add("ApiKey", "8e75a7f2-2b51-4360-b684-a14b1b233570");
            var response = await client.PostAsync(orderApiUrl, content);
            return response.IsSuccessStatusCode;
        }

        public async Task<List<LatestMovie>> GetLatestMovies()
        {
            var client = new HttpClient();
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, latestMoviesUrl);
            requestMessage.Headers.Add("ApiKey", "0c97bf09-4fbc-4395-ad41-d181191b1a52");
            var responseMessage = await client.SendAsync(requestMessage);
            var movieResponse = await responseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<LatestMovie>>(movieResponse);
        }
    }
}
