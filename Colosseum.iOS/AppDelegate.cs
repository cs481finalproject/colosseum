﻿using System;
using System.Collections.Generic;
using System.Linq;
using Syncfusion.ListView.XForms.iOS;

using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;


using Foundation;
using UIKit;

namespace Colosseum.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();

            //to launch the SFListView in iOS call the SfListViewRenderer.Init() within the FinishedLaunching overridden method 

            //make sure to call it after the Xamarin.Forms initialization and before the LoadApplication method is called
            SfListViewRenderer.Init();
            //used to track multiple custom events as well as catching & logging errors outside of defaults provided by analytics service
            AppCenter.Start("1f58d3b1-8d38-401f-86cf-b5acd808e7ee", typeof(Analytics), typeof(Crashes));
            AppCenter.Start("1f58d3b1-8d38-401f-86cf-b5acd808e7ee", typeof(Analytics), typeof(Crashes));
            UITabBar.Appearance.SelectedImageTintColor = UIColor.FromRGB(255, 87, 34);

            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }
    }
}
